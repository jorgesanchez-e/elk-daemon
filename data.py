import sys
import random
import time
import yaml
import syslog
import logging
import daemon
import threading
import Proc_oracle

class Get(daemon.Daemon):
    """
        Funcion encargada de obtener los datos de la base de datos, la --------
        configuracion inicial se debe colocar en el directorio a donde --------
        apunta la variable 'Home' y el archivo de configuracion se especifica -
        en la variable 'FConfig'
    """
    Home = "/etc/procesar-elk"
    FConfig = "config.yml"
    SecOracle = "ORACLE"
    SecQuerys = "QUERYS"
    Oracle = {}
    Querys = []
    OracleConn = None

    def __init__(self,filepid):
        syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_INFO)
        with open(self.Home + '/' + self.FConfig) as ymlfile:
            cfg = yaml.load(ymlfile)

        for section in cfg:
            if section == self.SecOracle:
               self.Oracle = self.parseOracle(cfg[self.SecOracle])
               logging.basicConfig(
                    filename=self.Oracle['log'], 
                    level=logging.INFO,
                    format='%(asctime)s: [%(levelname)s]: (%(threadName)-10s): %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                )
                
            elif section == self.SecQuerys:
               self.Querys = self.parseQuerys(cfg[self.SecQuerys],cfg)
            else:
                pass


        daemon.Daemon.__init__(self,filepid)

    def run(self):
        logging.info("Iniciando")

        msg = "Realizando conexion a la base de datos, [Ip:%s] [Puerto:%s] [Usuario:%s] [Servicio:%s]" % (
            self.Oracle['host'],
            self.Oracle['port'],
            self.Oracle['user'],
            self.Oracle['service'] )
        logging.info(msg)

        self.OracleConn = Proc_oracle.Oracle(
            self.Oracle['user'],
            self.Oracle['password'],
            self.Oracle['host'],
            self.Oracle['port'],
            self.Oracle['service'])

        self.OracleConn.conn()
	logging.info("Logeado a la base de datos version:[%s]" % self.OracleConn.version)

        ths = []
        for qdata in self.Querys:
            logging.info("Ejecutando hilo:%s" % qdata['name'])
            t = threading.Thread(target=self.getdata, args=(qdata['name'],), name=qdata['name'])
            t.setDaemon(True)
            ths.append(t)
            t.start()            

        while True:
            for t in ths:
                if not t.isAlive():
                    query_name = t.getName()
                    logging.info("El hilo de la consulta [%s] ha dejado de ejecutarse" % query_name)

            time.sleep(5)

    def getdata(self,name):
        """
            data_query = {
                "select": None,
                "fields": None,
                "output": None,
                "sqlite": None,
                "every":  None,
                "delay":  None,
                "name": None
            }
        """

        data_query = {}
        for data in self.Querys:
            if name == data['name']:
                data_query = data
                break

        while True:
            xsleep = int(data_query['every'])
            msg = "Ejecutancon consulta [%s] cada [%s] segundos con un delay de [%s] y salida hacia [%s]" % (
                    data_query['name'], 
                    data_query['every'], 
                    data_query['delay'], 
                    data_query['output']
                )
            logging.info(msg)
            cursor = None
	    cursor = self.OracleConn.getCursor()
            #cursor = self.OracleConn.query2(data_query['query'],{'finit':'08/06/2018 10:00:00', 'fend': '08/06/2018 10:00:05'})
            if cursor is None:
                logging.info("Error al obtener el cursor para la consulta")
            else:
                for xrow in self.OracleConn.next2(cursor):
                    logging.info(xrow[0])
            time.sleep(xsleep)



    def parseOracle(self,data):
        oracle_data = {
            "host":     None,
            "password": None,
            "user":     None,
            "service":  None,
            "port":     None,
            "log":      None
        }

        for key in data.keys():
            for key_ in oracle_data.keys():
                if key == key_:
                    oracle_data[key] = data[key]
        
        for key in oracle_data.keys():
            if oracle_data[key] is None:
                msg = "Error: No se ha especificado el valor de la variable [%s] en el bloque [%s], saliendo" % (key, "ORACLE")
                print msg
                syslog.syslog(msg)
                sys.exit(1)

        return oracle_data

    def parseQuerys(self,querys, data):
        data_querys = []

        for query in querys:
            data_query = {
                "select": None,
                "fields": None,
                "output": None,
                "sqlite": None,
                "every":  None,
                "delay":  None,
                "name": None
            }            
            for section in data:
                if section == query:
                    for key in data[section].keys():
                        for data_query_key in data_query.keys():
                            if key == data_query_key:
                                data_query[key] = data[section][key]
            
            for key in data_query.keys():
                if data_query[key] is None:
                    msg = "Error: No se ha especificado el valor de la variable [%s] en el bloque [%s], saliendo" % (key, query)
                    print msg
                    syslog.syslog(msg)
                    sys.exit(1)

            data_querys.append(data_query)

        return data_querys
