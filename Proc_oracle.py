import cx_Oracle
import logging
import sys

class Oracle:

    def __init__(self,user,password,host,port,service):
        self.myUser    = user
        self.myPasswd  = password
        self.myHost    = host
        self.myPort    = port
        self.myService = service
        self.myConn    = None
        self.myCursor  = None

    def __iter__(self):
        return self

    def conn(self):
        try:
            logging.info("Intentando conectar")
            self.MyStrConn = "%s/%s@%s:%s/%s" % (self.myUser, self.myPasswd, self.myHost, self.myPort, self.myService)
            self.MyConn = cx_Oracle.connect(self.MyStrConn)
        except cx_Oracle.DatabaseError as e:
            logging.info("Error al conectar con oracle %s" % e.args)
            sys.exit(2)
        logging.info("Conexion realizada")


    def query(self, query, values):

        if self.myConn == None:
            self.conn()

        if self.myCursor != None:
            self.myCursor.close()
        
        try:
            self.myCursor = self.MyConn.cursor()
            self.myCursor.execute(query, values)
        except cx_Oracle.DatabaseError as e:
            print("Error al ejecutar la consulta %s" % e.args)
            print ("Query: %s" % query)
            print values
            sys.exit(2)

    def getCursor(self):
	cursor = None
	logging.info("Obteniendo cursor")
	if self.myConn == None:
		logging.info("Desconexion detectada, intentando reconectar")
		#self.conn()

	#try:
	#	cursor = self.MyConn.cursor()
	#except cx_Oracle.DatabaseError as e:
	#	logging.info("No se pudo obtener el cursor: %s" % e.args)
	#	return cursor
	logging.info("Finalizando funcion de obtencion de cursor")
	return cursor

    def query2(self,query,dates):

        logging.info("Se ejecutara la consulta [%s]" % query)
        if self.myConn == None:
            logging.info("Desconexion detectada, intentando reconectar")
            self.conn()
        
        try:
            cursor = self.MyConn.cursor()
            cursor.execute(query,dates)
        except cx_Oracle.DatabaseError as e:
            logging.info("Error al ejecutar la consulta %s" % e.args)
            logging.info("Query: %s" % query)
            return None
        logging.info("Consulta ejecutada, regresando cursor")
        return cursor

    def next(self):
        if self.myCursor == None:
            raise StopIteration
            
        row = self.myCursor.fetchone()
        if row == None:
            self.myCursor.close()
            raise StopIteration
        return row

    def next2(self,cursor):
        if cursor == None:
            raise StopIteration
            
        row = cursor.fetchone()
        if row == None:
            cursor.close()
            cursor = None
            raise StopIteration
        return row

